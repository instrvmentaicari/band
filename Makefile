band:  ./band.c
	clang ./band.c -o ./band
install: band
	sudo cp ./band /usr/bin/
uninstall:
	sudo rm /usr/bin/band
clean:	band
	rm ./band
