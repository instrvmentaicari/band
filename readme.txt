Simple utility that just takes a band of each line set in the two arguments.
These two arguments represent the low and high index limit of each band.
These index from 1 to infinity, which is easily represented by any negative number.
This program uses exclusively UNIX pipes to complete its function, so you can script however you please.


	Example:

text_script.txt:

This is text script.
There is nothing special about it.
It is just used as an example.

command:

<text_script.txt band 2 10

output:

his is te
here is n
t is just

command:

<text_script.txt band 4 -1

output:

s is text script.
re is nothing special about it.
is just used as an example.
