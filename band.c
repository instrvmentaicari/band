#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "unistd.h"

int main (int argc, char** argv){
 unsigned int s= atoi (argv[1]), e= atoi (argv[2]), i= 1;
 char c;
 while (read (0, &c, 1)){
  if (i>=s&i<=e) write (1, &c, 1);
  i++;
  if (c=='\n') i= 1, write (1, "\n", 1);
 }
 return 0;
}
